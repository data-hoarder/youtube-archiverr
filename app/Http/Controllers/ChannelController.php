<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChannelController extends Controller
{
    function manage()
    {
        $output['_POST'] = $_POST;

        $name = $_POST['name'];
        $folder = $_POST['folder'];
        $url = $_POST['url'];

        $existingChannel = DB::table('channels')
            ->select()
            ->where('url', '=', $url)
            ->first();
        $output['existing'] = $existingChannel;

        DB::table('channels')->updateOrInsert([
            'url' => $url,
        ], [
            'name' => $name,
            'url' => $url,
            'folder' => $folder,
        ]);


        return $output;
    }
}
