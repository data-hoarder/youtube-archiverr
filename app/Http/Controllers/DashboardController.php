<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


class DashboardController extends Controller
{

    public function main()
    {
        $data = [];
        return view('dashboard/main', $data);
    }
}
