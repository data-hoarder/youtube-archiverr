<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>YouTube-dl manager!</title>
  </head>
  <body>

      <!-- As a heading -->
      <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">Youtube-dl Manager</span>
      </nav>


      <div class="container">
        <!-- Content here -->




        <form id="frmData" method="post" name="frmData">
            @csrf
            <div class="form-group">
              <label for="channelUrl">channelName</label>
              <input type="text" class="form-control" id="channelName" placeholder="Name of the Channel">
            </div>
            <div class="form-group">
              <label for="channelUrl">Channel URL</label>
              <input type="text" class="form-control" id="channelUrl" placeholder="https://www.youtube.com/channel/ <ID GOES HERE >/videos">
            </div>
            <div class="form-group">
              <label for="channelFolder">Target Folder</label>
              <input type="text" class="form-control" id="channelFolder" placeholder="reset">
            </div>
          <div class="d-flex">
            <button id="btnRemove" type="button" class="mr-auto p-2 btn btn-danger">Remove</button>
            <button type="button" class="p-2 mr-3 pl-5 pr-5 btn btn-light">Cancel</button>
            <button id="btnSubmit" type="button" class="p-2 pl-5 pr-5 btn btn-primary">Submit</button>
          </div>
        </form>





    </div><!-- .container -->









    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script>
$(document).ready(function() {
    $('#frmData').on('click', '#btnSubmit', function(e) {
        var token = $('input[name="_token"]').val();
        console.log('Click!');

        var postData = {
            _token: token,
            name: $('#channelName').val(),
            folder: $('#channelFolder').val(),
            url: $('#channelUrl').val(),
        };

        $.ajax({
    		method: "POST",
    		url: "/channel/manage",
    		cache: false,
    		data: postData
    	})
    	.done(function( msg ) {
    		console.log(["Data returned ", msg]);
    	});

    });
});
</script>

  </body>
</html>
